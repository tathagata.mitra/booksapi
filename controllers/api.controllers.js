const Joi = require('joi')
const { returnStatus } = require('../models/utils');
const { 
    validateBook, saveItemOnDatabase, addBookToMemory, 
    getItemsFromDatabase, getBookFromMemory, getBookList,
    validateEditBookRequest, editItemOnDatabase, 
    validateDeleteBookRequest, deleteItemFromDatabase
} = require('../models/api.model');

/**
 * Add a single book
 * @param {*} req 
 * @param {*} res 
 * @returns 
 */
const addBook = async (req, res) => {
    let data={} // Define the default value of return data

    // Validate the input parameters
    let {error, status_code, status_message} = validateBook(req.body)

    // If input data is fine then process to save the book information
    if(!error) {
        // Save book
        const result = await saveItemOnDatabase(req.body.book, addBookToMemory);

        // result from data save
        data = result.status_code && result.status_code === 200 ? result : { message : result.message }
        status_code = !result.data ? result.status_code : status_code
        status_message = result.data && result.data.book_id ? 'OK' : 'ERROR'
    }
    else {
        data = { message : status_message }
        status_message = 'Error'
    }

    // Return details
    return returnStatus(res, data, status_code, status_message)
}

/**
 * Get book list
 * @param {*} req 
 * @param {*} res 
 * @returns 
 */
const getBooks = async (req, res) => {
    status_code = 200

    // Get book records
    let list = await getItemsFromDatabase('books', getBookFromMemory)
    
    // Retrieve book name
    const records = await getBookList(list, 0, getBookList)
    
    // Return details
    return returnStatus(res, {records}, status_code, 'OK')
}

/**
 * Edit a single book ny name
 * @param {*} req 
 * @param {*} res 
 * @returns 
 */
const editBook = async(req, res) => {
    let data={} // Define the default value of return data

    // Validate the input parameters
    let {error, status_code, status_message} = validateEditBookRequest(req.body)

    // If input data is fine then process to save the book information
    if(!error) {
        // Save book
        const result = await editItemOnDatabase(req.body.original_book, req.body.new_book);

        // result from data save
        data = result.data || {}
        status_code = result.status_code || 200
        status_message = status_code === 200 ? 'OK' : 'ERROR'
    }
    else {
        data = { message : status_message }
        status_message = 'ERROR'
    }

    // Return details
    return returnStatus(res, data, status_code, status_message)
}

/**
 * Delete a single book by name
 * @param {*} req 
 * @param {*} res 
 * @returns 
 */
const deleteBook = async (req, res) => {

    let data={} // Define the default value of return data

    // Validate the input parameters
    let {error, status_code, status_message} = validateDeleteBookRequest(req.body)

    if(!error) {
        // Save book
        const result = await deleteItemFromDatabase(req.body.book);

        // result from data save
        data = result.data
        status_code = result.data ? result.status_code : status_code
        status_message = !result.data ? result.message : status_message
    }
    else {
        data = { message : status_message }
    }

    // Return details
    return returnStatus(res, data, status_code)
}

/**
 * Save a set of books
 * @param {*} req 
 * @param {*} res 
 * @returns 
 */
const putBook = async (req, res) => {
    let data={}, status_code = 200, status_message = 'OK' // Define the default value of return data

    // Validate the input parameters
    if(!req.body.books || !req.body.books.length === 0) {
        data = { message : 'Invalid book list' }
        status_code = 400
    }
    else {
        const status = []
        for (let name of req.body.books) {
            name = name.trim()
            let book_id, time, message
            if(name) {
                const {data, message} = await saveItemOnDatabase(name, addBookToMemory);
                book_id = data && data.book_id ? data.book_id : 0
                time = data && data.time ? data.time : 0
                msg = message || 'Unable to save'
            }
            
            const key = name.toString(); 
            status[key] = book_id && time ? time : (msg || 'Unable to save')   
        }
        data = status
    }

    // Return details
    return returnStatus(res, data, status_code, status_message)
}

module.exports = { addBook, getBooks, editBook, deleteBook, putBook }