const Joi = require('joi');
const cache = require('memory-cache');
const { getJsonFromStr } = require('./utils')

/**
 * Validate book name
 * @param {*} params 
 * @returns 
 */
const validateBook = (params) => {
  const JoiSchema = Joi.object({
    book: Joi.string()
          .min(5)
          .max(50)
          .required()
          .error(() => {
            return { message: 'Please enter a valid book name between 5 to 50 characters.' };
          }),
  }).options({ abortEarly: false });
  const response = JoiSchema.validate(params)
  return response.error ?  
          { 
              error: response.error, 
              status_code: 400, 
              status_message: response.error.details[0].message 
          } : {
            error:false, 
            status_code: 200,
            status_message: 'OK'
          }
}

/**
 * Validate edit book request
 * @param {*} params 
 * @returns 
 */
const validateEditBookRequest = (params) => {
  const JoiSchema = Joi.object({
    original_book: Joi.string()
          .min(5)
          .max(50)
          .required()
          .error(() => {
            return { message: 'The Original book name field must contain valid data.' };
          }),
    new_book: Joi.string()
          .disallow(Joi.ref('original_book'))
          .min(5)
          .max(50)
          .required()
          .error(() => {
            return { message: 'The New book name must contain valid data and be different from the original name.' };
          }),
  }).options({ abortEarly: false });
  const response = JoiSchema.validate(params)
  return response.error ?  
          { 
              error: response.error, 
              status_code: 400, 
              status_message: response.error.details[0].message 
          } : {
            error:false, 
            status_code: 200,
            status_message: 'OK'
          }
}

/**
 * Validate delete book request
 * @param {*} params 
 * @returns 
 */
const validateDeleteBookRequest = (params) => {
  const JoiSchema = Joi.object({
    book: Joi.string()
          .min(5)
          .max(50)
          .required()
          .error(() => {
            return { message: 'Please enter a valid book name to delete.' };
          })
  }).options({ abortEarly: false });
  const response = JoiSchema.validate(params)
  return response.error ?  
          { 
              error: response.error, 
              status_code: 400, 
              status_message: response.error.details[0].message 
          } : {
            error:false, 
            status_code: 200,
            status_message: 'OK'
          }
}

/**
 * Save a single item into DB
 * @param {*} book_name 
 * @param {*} callback 
 * @returns 
 */
const saveItemOnDatabase = async (book_name, callback) => {
  const time = Math.floor((Math.random() * 200) + 500 + (book_name.length * 10));

  let stat = await new Promise(function(resolve, reject) { 
    setTimeout(() => {
        resolve(callback(book_name));
    }, time)
  });
  
  if(stat && stat.book_id && stat.book_name) {
    return { 
      message: "Book added successfully.",
      data : { "book_id": stat.book_id, "name": stat.book_name, time }
    }
  }
  return {
    message : stat.message || 'Invalid data',
    status_code: 404
  }
}

/**
 * Add book into memory
 * @param {*} book_name 
 * @returns 
 */
const addBookToMemory = async (book_name) => {
  // default error message and books data
  let message = 'Book name already exists.'
  let books_record = false

  // Default book ID
  book_id = 1

  // Get existing book information from memory
  const books = cache.get('books');

  // Get existing data from JSON string
  if(books) 
    books_record = getJsonFromStr(books)

  if(books_record && books_record.length > 0) {

    // Get max index
    for (const row of books_record) {
      book_id = row.book_id + 1
      if(book_name === row.book_name) {
        return { book_id: false, book_name, message };
      }
    }

    // Add new book name at the end of list
    books_record.push({ book_id, book_name })
  }
  else 
    books_record = [{ book_id, book_name }]

  cache.put('books', JSON.stringify(books_record));
  return { book_id, book_name, message };
}

/**
 * Get book from memory
 * @param {*} table 
 * @returns 
 */
const getBookFromMemory = async(table) => {
  return getJsonFromStr(cache.get(table)) || []
}

/**
 * Get item from database
 * @param {*} table 
 * @param {*} callback 
 * @returns 
 */
const getItemsFromDatabase = async (table, callback) => {
  return await callback(table)
}

/**
 * Get book list
 * @param {*} list 
 * @param {*} index 
 * @param {*} callback 
 * @param {*} names 
 * @returns 
 */
const getBookList = async (list, index, callback, names = []) => {
  if(list && list[index] && list[index].book_name) {
    names.push(list[index].book_name)
    return await callback(list, index+1, callback, names)    
  }
  return names
}

/**
 * Edit item on database
 * @param {*} original_book 
 * @param {*} new_book 
 * @returns 
 */
const editItemOnDatabase = async (original_book, new_book) => {
  let status_code = 404, status_message = 'Book name not available on database'
  let books_record = await getBookFromMemory('books')
  let index = 0
  if(books_record && books_record.length > 0) {
    for (const row of books_record) {
      if(row.book_name === original_book) {
        status_code = 200
        status_message = 'Original book name updated with new name.'
        books_record[index].book_name = new_book
      }
      index++
    }
  }

  if(status_code === 200) {
    cache.put('books', JSON.stringify(books_record));
  }

  return {
    data: { original_book, new_book, message: status_message }, 
    status_code
  };
}

/**
 * Delete item from database
 * @param {*} book_name 
 * @returns 
 */
const deleteItemFromDatabase = async (book_name) => {
  status_code = 404
  status_message = 'Unable to find book in database.'

  let books_record = await getBookFromMemory('books')
  const book_new_record = []
  if(books_record && books_record.length > 0) {
    for (const row of books_record) {
      if(row.book_name === book_name) {
        status_code = 200
        status_message = 'Specified book name removed from database.'
      }
      else 
        book_new_record.push(row)
    }
  }

  if(status_code === 200) {
    cache.put('books', JSON.stringify(book_new_record));
  }

  return {
    data: { book_name, message: status_message }, 
    status_code
  };
}


module.exports = { 
    validateBook, saveItemOnDatabase, addBookToMemory, 
    getItemsFromDatabase, getBookFromMemory, getBookList, 
    validateEditBookRequest, editItemOnDatabase, 
    validateDeleteBookRequest, deleteItemFromDatabase
}