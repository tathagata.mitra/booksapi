
/**
 * Get return status
 * @param {*} res 
 * @param {*} data 
 * @param {*} status_code 
 * @param {*} status_message 
 * @returns 
 */
const returnStatus = (res, data, status_code = 404, status_message='ERROR') => {
    if(!data) return res.status(status_code || 404).send({status:'ERROR', 'message': status_message});
    return res.status(status_code).send({status:status_message, ...data});
}

/**
 * Validate if given string is JSON
 * @param {*} str 
 * @returns 
 */
const getJsonFromStr = (str) => {
    try {
        return JSON.parse(str);
    } catch (e) {
        return false;
    }
}

module.exports = { returnStatus, getJsonFromStr }