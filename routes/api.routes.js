const  express = require('express');
const router = express.Router();
const api = require('../controllers/api.controllers');

router.post("/",    api.addBook);     // Add a single book
router.get("/",     api.getBooks);    // Get list of available books
router.patch("/",   api.editBook);    // Edit a single book by name
router.delete("/",  api.deleteBook);  // Delete a single book by name
router.put("/",     api.putBook);     // Add a set of books

module.exports = router;