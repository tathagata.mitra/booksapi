const home = require('./routes/home.routes');
const api_path = require('./routes/api.routes');
const cluster = require('cluster')
const os = require('os')
const config = require('config');
const cors = require('cors');
const express = require('express');
const app = express();

require('./startup/prod')(app);
const numCPU = os.cpus().length

// add cors
app.use(cors());
app.options('*', cors());

// Load body parser
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Load routers
app.use('/', home);
app.use('/books', api_path);
app.use('/static', express.static('public'))

if(!process.env.AWS_LAMBDA_FUNCTION_NAME || process.env.AWS_LAMBDA_FUNCTION_NAME === '') {
    const port = process.env.PORT || 8888;

    if(config.get('enableCluster') && cluster.isMaster) {
        for(let i = 0; i < numCPU; i++) {
            cluster.fork()
        }
        cluster.on('exit', (worker, code, signal)=> {
            console.log(`Worker ${process.pid} died.`)
            cluster.fork()
        })
    }
    else {
        app.listen(port, () => console.log(`Listning on port ${port}` 
        + (config.get('enableCluster') ? `on ${process.pid}` : ``) 
        + `.....`));
    }

    app.timeout = 0; //Set to 0 to disable any kind of automatic timeout behavior on incoming connections..
}

module.exports = app;