const { 
    validateBook, validateEditBookRequest, validateDeleteBookRequest, 
    saveItemOnDatabase, addBookToMemory, getBookFromMemory, getItemsFromDatabase,
    getBookList, editItemOnDatabase, deleteItemFromDatabase
} = require('../models/api.model')

// Define test name of book which wiull be removed at last
const test_book_name = 'nd7298sms3ma94nvx3m', 

// name of the table
table = 'books'

// 1.0 Verify book name validation
test('Book name validation test', async () => {
    let status

    // Success scenario: test of proper name pass
    status = await validateBook({book:'Hello'})
    if(status.status_code && status.status_code !== 200) {
        throw new Error('Book name validation failed if name is proper')
    }

    // Failure scenario: test of small name pass
    status = await validateBook({book:'12'})
    if(status.status_code && status.status_code !== 400) {
        throw new Error('Book name validation failed if name is proper')
    }

    // Failure scenario: test of large name pass
    status = await validateBook({book:'123456789012345678901234567890123456789012345678901234567890'})
    if(status.status_code && status.status_code !== 400) {
        throw new Error('Book name validation failed if name is proper')
    }
})

// 2.0 Verify book name validation during editing
test('Book name editing validation failed.', async () => {
    let status

    // Success scenario: Validate small book name
    status = await validateEditBookRequest({
        "original_book": "In Search of Lost Time",
        "new_book": "Lost Time"
    })
    if(status.status_code && status.status_code !== 200) {
        throw new Error('Book name edit validation failed')
    }

    // Failure scenario: Validate small book name
    status = await validateEditBookRequest({
        "original_book": "Lost Time",
        "new_book": "Lost Time"
    })
    if(status.status_code && status.status_code !== 400) {
        throw new Error('Book name edit validation failed')
    }
})

// 3.0 Verify book name validation during deletion
test('Book name delete validation', async () => {
    let status

    // Success scenario: Validate with valid book name to delete
    status = validateDeleteBookRequest({"book" : "Garfield"})
    if(status.status_code && status.status_code !== 200) {
        throw new Error('Delete book name validation failed when name is proper')
    }

    // Failure scenario: Validate with invalid book name to delete
    status = validateDeleteBookRequest({"book" : "Gar"})
    if(status.status_code && status.status_code !== 400) {
        throw new Error('Delete book name validation failed when name is small')
    }
})

// 4.0 Verify save item to DB
test('Validate same item on database', async () => {
    let status

    // Success scenario: Save a new item to DB 
    status = await saveItemOnDatabase(test_book_name, addBookToMemory)
    if(!status || !status.data || !status.data.book_id)
        throw new Error('Unable to save book into DB')

    // Failure scenario: Save an item to DB which is already exists
    status = await saveItemOnDatabase(test_book_name, addBookToMemory)
    if(!status || (status.data && status.data.book_id))
        throw new Error('Save wrong book name which is already exists on DB')
})

// 5.0 Save item on memory directly
test('Validate same item on memory directly', async () => {
    let status

    // Success scenario: Try to save same book name
    status = await addBookToMemory(test_book_name)    
    if(!status || (status && status.book_id !== false))
        throw new Error('Same book name cannot be saved twice')

    // Failure scenario: Try to save diffrent book name
    status = await addBookToMemory(test_book_name + '_1')    
    if(!status || (status && !status.book_id))
        throw new Error('Unable to save new book name.')
})

// 6.0 Retrieve saved book name from DB
test('Get saved book names from memory', async () => {
    let status

    // Success scenario: Get books from correct table
    status = await getBookFromMemory(table)
    if(!status || status.length !== 2 || status[0].book_name !== test_book_name)
        throw new Error('Unable to retrive saved book from memory')

    // Failure scenario: Get book from incorrect DB
    status = await getBookFromMemory(table + '_text')
    if(status && status.length > 0)
        throw new Error('Received wrong book from memory')
})

// 7.0 Get list of available items form specific table of DB
test('Get items from database', async () => {
    let status

    // Success scenario: Get list of available from correct table
    status = await getItemsFromDatabase(table, getBookFromMemory)
    if(!status || status.length !== 2 || status[0].book_name !== test_book_name)
        throw new Error('Unable to retrive saved book from database')

    // Failure scenario: Get list of available from wrong table
    status = await getItemsFromDatabase(table + '_dummy', getBookFromMemory)
    if(status && status.length > 0)
        throw new Error('Get wrong items from database')
})

// 8.0 Get list of books available on DB in a specific format using callback
test('Get book list', async () => {
    let list, records
   
    // Success scenario: Retrieve correct book name from correct table
    list = await getItemsFromDatabase(table, getBookFromMemory)
    records = await getBookList(list, 0, getBookList)
    if(!records || records.length !== 2 || records[0] !== test_book_name) {
        throw new Error('Unable to retrieve book list')
    }

    // Failure scenario: Retrieve book name from wrong table
    list = await getItemsFromDatabase(table + '_dummy', getBookFromMemory)
    records = await getBookList(list, 0, getBookList)
    if(records && records.length > 0) {
        throw new Error('Unable to retrieve book list')
    }
})

// 9.0 Verify method to edit item from DB
test('Verify edit item on database', async () => {
    let status

    // Success scenario: Validate edit correct items on database
    status = await editItemOnDatabase(test_book_name+'_1', test_book_name+'_2')
    if(!status || (status && status.status_code !== 200)) {
        throw new Error('Unable to edit correct items on database')
    }

    // Failure scenario: Validate edit wrong items on database
    status = await editItemOnDatabase(test_book_name+'_20', test_book_name+'_21')
    if(!status || (status && status.status_code !== 404)) {
        throw new Error('Items got wrongly edited which is not available on database.')
    }
})

// 10.0 Verify deleting items from DB
test('Test item deletion from database', async () => {
    let status

    // Success scenario: Validate deleting correct items from DB
    status = await deleteItemFromDatabase(test_book_name);
    status = await deleteItemFromDatabase(test_book_name+'_2');
    if(!status || status.status_code !== 200) {
        throw new Error('Unable to remove correctly from database.')
    }

    // Failure scenario: Validate deleting wrong items from DB
    status = await deleteItemFromDatabase(test_book_name+'_3');
    if(!status || status.status_code !== 404) {
        throw new Error('Item deletion is not correct.')
    }
})